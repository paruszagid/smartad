<?
use app\sitebuilder\SiteBuilder;
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?=$this->title?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/assets/materialize/css/materialize.min.css">
        <script src="/assets/jquery/jquery.min.js"></script>
        <script src="/assets/materialize/js/materialize.js"></script>
        <style>
            .content {
                min-height: calc(100vh - 154px);
            }
        </style>
    </head>
<body>
<div class="wrapper">
    <nav class="blue-grey navbar-fixed">
        <div class="nav-wrapper">
            <div class="brand-logo"><a href="/administrator" class="thin"><?=SiteBuilder::$app->siteName?> | <?=$this->title?></a> </div>
            <a href="#" data-activates="mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

            <ul id="recommendation" class="dropdown-content">
                <li><a href="/administrator/recommendations/categories">Категории</a></li>
                <li class="divider"></li>
                <li><a href="/administrator/recommendations/products">Товары</a></li>
            </ul>

            <ul class="right hide-on-med-and-down">
                <li><a href="/administrator/companies">Компании</a></li>
                <li><a href="/administrator/adword">Рекламма</a></li>
                <li><a class="dropdown-button" href="#!" data-activates="recommendation">Рекомендации <i class="mdi-navigation-arrow-drop-down right"></i></a></li>
                <li><a href="/administrator/sign_out">Выход</a></li>
            </ul>

            <ul class="side-nav" id="mobile">
                <li><a href="/administrator/clients">Клієнти</a></li>
                <li><a href="/administrator/admins">Адміністратори</a></li>
                <li><a href="/administrator/companies">Компанії</a></li>
            </ul>
        </div>
    </nav>

    <div class="content">
        <?=$this->content?>
    </div>

    <footer class="page-footer blue-grey">
        <div class="footer-copyright">
            <?=SiteBuilder::$app->siteName?> &copy; <?=date('Y')?>

            <a class="right white-text" href="http://vk.com/serrg1994">Novoseletskiy Serhiy</a>
        </div>
    </footer>
</div>

<script>
    $('select').material_select();
    $('.materialboxed').materialbox();
    $('#email').attr('type', 'email');
    $(".dropdown-button").dropdown();
</script>
</body>
</html>