<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 21:59
 */

namespace modules\admin;


use app\sitebuilder\Module;

class AdminModule extends Module
{

    function route()
    {
        return [
            '^/$' => [
                'controller' => 'modules\admin\controllers\AdminController',
                'action' => 'mainPage'
            ],

            '^companies/$' => [
                'controller' => 'modules\admin\controllers\CompanyController',
                'action' => 'list'
            ],

            '^companies/(?P<action>\w+)/(?P<id>\d+)/$' => [
                'controller' => 'modules\admin\controllers\CompanyController',
            ],

            '^companies/(?P<action>\w+)/$' => [
                'controller' => 'modules\admin\controllers\CompanyController',
            ],

            '^adword/$' => [
                'controller' => 'modules\admin\controllers\AdwordController',
                'action' => 'list'
            ],

            '^adword/(?P<action>\w+)/$' => [
                'controller' => 'modules\admin\controllers\AdwordController',
            ],

            '^adword/(?P<action>\w+)/(?P<id>\d+)/$' => [
                'controller' => 'modules\admin\controllers\AdwordController',
            ],

            '^recommendations/categories/$' => [
                'controller' => 'modules\admin\controllers\CategoryController',
                'action' => 'list'
            ],

            '^recommendations/categories/(?P<action>\w+)/(?P<id>\d+)/$' => [
                'controller' => 'modules\admin\controllers\CategoryController',
            ],

            '^recommendations/categories/(?P<action>\w+)/$' => [
                'controller' => 'modules\admin\controllers\CategoryController',
            ],

            '^recommendations/products/$' => [
                'controller' => 'modules\admin\controllers\ProductController',
                'action' => 'list'
            ],

            '^recommendations/products/(?P<action>\w+)/(?P<id>\d+)/$' => [
                'controller' => 'modules\admin\controllers\ProductController',
            ],

            '^recommendations/products/(?P<action>\w+)/$' => [
                'controller' => 'modules\admin\controllers\ProductController',
            ],

            '^(?P<action>\w+)' => [
                'controller' => 'modules\admin\controllers\AdminController'
            ],

            '^post/(?P<id>\d+)/$' => [
               'POST' => [
                   'controller' => 'modules\admin\controllers\CompanyController',
                   'action' => 'updatePost'
               ]
            ],

            '^upload_banner/(?P<id>\d+)/$' => [
                'POST' => [
                    'controller' => 'modules\admin\controllers\CompanyController',
                    'action' => 'uploadBanner'
                ]
            ],

            '^banner/(?P<id>\d+)/$' => [
                'controller' => 'modules\admin\controllers\CompanyController',
                'action' => 'banner'
            ],
        ];
    }
}