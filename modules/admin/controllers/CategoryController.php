<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 24.09.15
 * Time: 10:03
 */

namespace modules\admin\controllers;


use app\helpers\FileManager;
use app\sitebuilder\ModuleController;
use models\Category;
use modules\admin\forms\CategoryForm;

class CategoryController extends ModuleController
{
    function listAction()
    {
        return $this->render('category/list', [
            'categories' => Category::getAll(['orderBy' => 'id', 'orderType' => 'DESC'])
        ]);
    }

    function addAction()
    {
        return $this->render('category/form', [
            'form' => new CategoryForm()
        ]);
    }

    function saveAction()
    {
        $form = new CategoryForm($_POST);
        if ($form->is_valid()) {
            $form->save();
            $this->redirect('/administrator/recommendations/categories');
        } else
            return $this->render('category/form', [
                'form' => $form
            ]);
    }

    function editAction($id)
    {
        return $this->render('category/form', [
            'form' => new CategoryForm(null, Category::getByPK($id))
        ]);
    }

    function updateAction($id)
    {
        $category = Category::getByPK($id);


        $form = new CategoryForm($_POST, $category);

        if ($form->is_valid())
            $form->save();

        $this->redirect('/administrator/recommendations/categories/edit/' . $category->id);
    }

    function removeAction($id) {
        $category = Category::getByPK($id);


        $category->delete();
        $this->redirect('/administrator/recommendations/categories');
    }
}