<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 22:20
 */

namespace modules\admin\controllers;


use app\helpers\FileManager;
use app\helpers\VarDumper;
use app\sitebuilder\ModuleController;
use app\sitebuilder\SiteBuilder;
use models\Banner;
use models\Client;
use models\Company;
use models\Post;
use modules\admin\forms\CompanyForm;
use modules\admin\forms\PostForm;

class CompanyController extends ModuleController
{
    function listAction()
    {
        return $this->render('company/list', [
            'companies' => Company::getAll(['orderBy' => 'id', 'orderType' => 'DESC'])
        ]);
    }

    function editAction($id)
    {
        $company = Company::getByPK($id);
        $form = new CompanyForm(null, $company);
        $post = new PostForm(null, Post::where(['company_id' => $id])[0]);
        $banners = Banner::where(['company_id' => $company->id]);

        return $this->render('company/form', [
            'form' => $form,
            'post' => $post,
            'banners' => $banners
        ]);
    }

    function updateAction($id)
    {
        $company = Company::getByPK($id);


        $_FILES['image']['type'] = strtolower($_FILES['image']['type']);

        if ($_FILES['image']['type'] == 'image/png'
            || $_FILES['image']['type'] == 'image/jpg'
            || $_FILES['image']['type'] == 'image/gif'
            || $_FILES['image']['type'] == 'image/jpeg'
            || $_FILES['image']['type'] == 'image/pjpeg'
        ) {

            if (file_exists(__DIR__ . '/../../../content/companies/' . $company->image))
                unlink(__DIR__ . '/../../../content/companies/' . $company->image);

            $company->image = md5(microtime()) . '.jpg';
            move_uploaded_file($_FILES['image']['tmp_name'], __DIR__ . '/../../../content/companies/' . $company->image);
        }

        $form = new CompanyForm($_POST, $company);

        if ($form->is_valid()) {
            $form->save();
            $this->redirect('/administrator/companies/edit/' . $id);
        } else {
            return $this->render('company/form', [
                'form' => $form
            ]);
        }
    }

    function updatePostAction($id)
    {
        $post = Post::getByPK($id);

        $image = FileManager::uploadImage('image', __DIR__ . '/../../../content/posts/');

        if ($image) {
            FileManager::deleteFile(__DIR__ . '/../../../content/posts/' . $post->image);
            $post->image = $image;
        }


        if ($_FILES['audio']['name']) {
            if (file_exists(__DIR__ . '/../../../content/posts/audio/' . $post->audio))
                unlink(__DIR__ . '/../../../content/posts/audio/' . $post->audio);

            $post->audio = md5($_FILES['audio']['name']) . '.mp3';
            move_uploaded_file($_FILES['audio']['tmp_name'], __DIR__ . '/../../../content/posts/audio/' . $post->audio);
        }

        $form = new PostForm($_POST, $post);

        if ($form->is_valid())
            $form->save();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }


    function uploadBannerAction($id)
    {
        $image = FileManager::uploadImage('image', SiteBuilder::$app->companyBannersPath);
        if ($image) {
            $banner = new Banner();
            $banner->url = $_POST['url'];
            $banner->image = $image;
            $banner->company_id = $id;
            if ($banner->is_valid())
                $banner->save();
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function bannerAction($id)
    {
        switch (SiteBuilder::$app->request->method) {
            case 'POST': {
                $banner = Banner::getByPK($id);
                $banner->url = $_POST['url'];
                $banner->save();
            }
                break;

            case 'GET': {
                $banner = Banner::getByPK($id);
                FileManager::deleteFile(SiteBuilder::$app->companyBannersPath . $banner->image);
                $banner->delete();
            }
                break;
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function addAction()
    {
        return $this->render('company/form', [
            'form' => new CompanyForm()
        ]);
    }

    function saveAction()
    {
        $form = new CompanyForm($_POST);

        $form->instance->date_begin = date("Y-m-d", time());

        $_FILES['image']['type'] = strtolower($_FILES['image']['type']);

        if ($_FILES['image']['type'] == 'image/png'
            || $_FILES['image']['type'] == 'image/jpg'
            || $_FILES['image']['type'] == 'image/gif'
            || $_FILES['image']['type'] == 'image/jpeg'
            || $_FILES['image']['type'] == 'image/pjpeg'
        ) {


            $form->instance->image = md5(microtime()) . '.jpg';
            move_uploaded_file($_FILES['image']['tmp_name'], __DIR__ . '/../../../content/companies/' . $form->instance->image);

        }

        if ($form->is_valid()) {
            $form->save();

            $post = new Post();
            $post->company_id = $form->instance->id;
            $post->save();

            $this->redirect('/administrator/companies/edit/' . $form->instance->id);
        } else {
            $this->redirect('/administrator/companies/');
        }


    }

    function removeAction($id)
    {
        Post::deleteWhere(['company_id' => $id]);
        $company = Company::getByPK($id);

        Client::deleteWhere(['id' => $company->client_id]);
        $company->delete();

        return $this->redirect('/administrator/companies/');
    }
}