<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 24.09.15
 * Time: 10:46
 */

namespace modules\admin\controllers;


use app\helpers\FileManager;
use app\sitebuilder\ModuleController;
use models\Category;
use models\CategoryProduct;
use models\Product;
use modules\admin\forms\ProductForm;

class ProductController extends ModuleController
{
    function listAction()
    {
        return $this->render('product/list', [
            'products' => Product::getAll(['orderBy' => 'id', 'orderType' => 'DESC'])
        ]);
    }

    function addAction()
    {
        return $this->render('product/form', ['form' => new ProductForm()]);
    }

    function editAction($id)
    {
        return $this->render('product/form', [
            'form' => new ProductForm(null, Product::getByPK($id)),
        ]);
    }

    function saveAction()
    {
        $image = FileManager::uploadImage('image', __DIR__ . '/../../../content/products/');
        if ($image) {
            $_POST['image'] = $image;
        }

        $form = new ProductForm($_POST);
        $form->save();

        $this->redirect('/administrator/recommendations/products/edit/'.$form->instance->id);
    }

    function updateAction($id)
    {
        $product = Product::getByPK($id);
        $image = FileManager::uploadImage('image', __DIR__ . '/../../../content/products/');

        if ($image) {
            FileManager::deleteFile(__DIR__ . '/../../../content/categories/' . $product->image);
            $product->image = $image;
        }

        $form = new ProductForm($_POST, $product);

        if ($form->is_valid())
            $form->save();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }


    function removeAction($id)
    {
        $product = Product::getByPK($id);

        if ($product->image) {
            FileManager::deleteFile(__DIR__ . '/../../../content/products/' . $product->image);
        }

        $product->delete();
        $this->redirect('/administrator/recommendations/products');
    }
    
}