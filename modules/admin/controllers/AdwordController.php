<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 09.10.15
 * Time: 20:43
 */

namespace modules\admin\controllers;

use app\helpers\FileManager;
use app\sitebuilder\Database;
use app\sitebuilder\ModuleController;
use app\sitebuilder\SiteBuilder;
use models\Adword;
use models\AdwordCompany;
use models\Company;
use models\MediaPlan;
use modules\admin\forms\AdwordForm;

class AdwordController extends ModuleController
{
    function listAction()
    {
        return $this->render('adword/list', [
            'adwords' => Adword::getAll(['orderBy' => 'id', 'orderType' => 'DESC'])
        ]);
    }

    function addAction()
    {
        return $this->render('adword/form', [
            'form' => new AdwordForm()
        ]);
    }

    function editAction($id)
    {
        $adword = Adword::getByPK($id);
        $form = new AdwordForm(null, $adword);

        return $this->render('adword/form', [
            'form' => $form,
            'media_plan' => MediaPlan::where(['adword_id' => $id]),
            'companies' => Company::getAll(),
            'adword_company' => SiteBuilder::$app->db->getResultQuery(
                "SELECT ac.id, c.name, ac.access FROM companies c JOIN adword_company ac ON c.id = ac.company_id WHERE ac.adword_id = $id",
                Database::$RESULT_OBJECT
            )
        ]);
    }

    function saveAction()
    {
        $image = FileManager::uploadImage('image', SiteBuilder::$app->adwordImagePath);
        if ($image)
            $_POST['image'] = $image;

        $audio = FileManager::uploadFile('audio', SiteBuilder::$app->adwordAudioPath);
        if ($audio)
            $_POST['audio'] = $audio;

        $form = new AdwordForm($_POST);
        if ($form->is_valid()) {
            $form->save();
            $this->redirect('/administrator/adword/edit/' . $form->instance->id);
        } else {
            return $this->render('adword/form', [
                'form' => $form
            ]);
        }
    }

    function updateAction($id)
    {
        $adword = Adword::getByPK($id);

        $image = FileManager::uploadImage('image', SiteBuilder::$app->adwordImagePath);
        if ($image) {
            $_POST['image'] = $image;
            FileManager::deleteFile(SiteBuilder::$app->adwordImagePath . $adword->image);
        }

        $audio = FileManager::uploadFile('audio', SiteBuilder::$app->adwordAudioPath);
        if ($audio) {
            $_POST['audio'] = $audio;
            FileManager::deleteFile(SiteBuilder::$app->adwordAudioPath . $adword->audio);
        }

        $form = new AdwordForm($_POST, $adword);
        if ($form->is_valid())
            $form->save();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function add_mediaAction($id)
    {
        $mediaPlan = new MediaPlan();
        $mediaPlan->date_begin = strtotime($_POST['date_begin']);
        $mediaPlan->date_end = strtotime($_POST['date_end']);
        $mediaPlan->adword_id = $id;

        $mediaPlan->save();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function remove_mediaAction($id)
    {
        MediaPlan::deleteWhere(['id' => $id]);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function remove_imageAction($id)
    {
        $adword = Adword::getByPK($id);

        if ($adword->image) {
            FileManager::deleteFile(SiteBuilder::$app->adwordImagePath . $adword->image);
            $adword->image = null;
            $adword->save();
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function remove_audioAction($id)
    {
        $adword = Adword::getByPK($id);

        if ($adword->audio) {
            FileManager::deleteFile(SiteBuilder::$app->adwordAudioPath . $adword->audio);
            $adword->audio = null;
            $adword->save();
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function mediaAction()
    {
        return $this->render('adword/media');
    }

    function eventsAction()
    {
        $query = "SELECT DISTINCT m.*, a.title FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id";

        $adwords = SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT);
        return json_encode($adwords);
    }

    function change_eventAction($id)
    {
        $media = MediaPlan::getByPK($id);
        $media->date_begin = strtotime($_POST['date_begin']);
        $media->date_end = strtotime($_POST['date_end']);
        $media->save();
        return $this->eventsAction();
    }

    function create_eventAction()
    {
        $media = new MediaPlan();
        $media->date_begin = strtotime($_POST['date_begin']);
        $media->date_end = strtotime($_POST['date_end']);
        $media->adword_id = $_POST['adword_id'];
        $media->save();
        return $this->eventsAction();
    }

    function delete_eventAction($id)
    {
        MediaPlan::deleteWhere(['id' => $id]);
        return $this->eventsAction();
    }

    function delta_eventAction($id)
    {
        $media = MediaPlan::getByPK($id);
        $media->date_begin += $_POST['delta'];
        $media->date_end += $_POST['delta'];
        $media->save();

        SiteBuilder::$app->cache->set('fdf', $media);

        return $id;
    }

    function adwordsAction()
    {
        return json_encode(Adword::getAll(['orderBy' => 'id', 'orderType' => 'DESC']));
    }


    function add_companyAction($id)
    {
        $adwordCompany = new AdwordCompany($_POST);
        $adwordCompany->adword_id = $id;
        $adwordCompany->save();
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    function remove_companyAction($id) {
        AdwordCompany::deleteWhere(['id' => $id]);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}