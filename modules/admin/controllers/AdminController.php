<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 22:03
 */

namespace modules\admin\controllers;


use app\sbuser\User;
use app\sitebuilder\Database;
use app\sitebuilder\ModuleController;
use app\sitebuilder\SiteBuilder;
use models\Company;

class AdminController extends ModuleController
{
    function mainPageAction() {
        $time = time();

        $query =  "SELECT DISTINCT a.* FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id
                WHERE m.date_begin <= $time and m.date_end >= $time";
        return $this->render('home',[
            'companies' => Company::getAll(['orderBy'=>'id', 'orderType' => 'desc', 'limit' => 5]),
            'adwords' => SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT)
        ]);
    }

    function loginAction() {
        return $this->ajaxRender('login');
    }

    function sign_inAction()
    {
        $client = new User($_POST);
        $client->sign_in();

        $this->redirect('/administrator');
    }

    function sign_outAction() {
        $client = new User();
        $client->sign_out();
        $this->redirect('/administrator');
    }
}