<?
/**
 * @var \modules\admin\forms\CompanyForm $form
 */

$this->title = ($form->instance->name ? $form->instance->name : 'Новая компания');
?>


<div class="row">
    <div class="col m8">
        <div class="card card-panel">
            <a class="btn right" target="_blank" href="/adword/<?=$form->instance->alias?>">
                <i class="mdi-image-remove-red-eye"></i> Смотреть рекламму
            </a>


            <h3 class="card-title black-text">Основная информация о компании</h3>
            <form method="post" action="<?= ($form->instance->name ?
                '/administrator/companies/update/' . $form->instance->id :
                '/administrator/companies/save/') ?>" enctype="multipart/form-data">
                <div class="col m4">
                    <?= $form->field('name') ?>
                    <?= $form->field('alias') ?>

                    <label>Логотип</label>
                    <? if ($form->instance->image): ?>
                        <img src="/content/companies/<?= $form->instance->image ?>" style="max-width: 100%;">
                    <? endif ?>
                    <input type="file" name="image" accept="image/*">
                </div>

                <div class="col m4">
                    <?= $form->field('shop') ?>
                    <?= $form->field('site') ?>
                    <?= $form->field('fb') ?>
                    <?= $form->field('vk') ?>
                </div>

                <div class="col m4">
                    <?= $form->field('date_begin', ['disabled' => true]) ?>
                    <?= $form->field('date_end')?>
                    <?= $form->field('telephone')?>
                    <?= $form->field('email')?>
                </div>

                <div class="col m12">
                    <button type="submit" class="btn btn-success right">Сохранить</button>
                </div>
            </form>
        </div>

       <?if($form->instance->id):?>
           <div class="card card-panel">
               <h3 class="card-title black-text">Рекламные баннера</h3>

               <form method="post" action="/administrator/upload_banner/<?= $form->instance->id ?>"
                     enctype="multipart/form-data">
                   <div class="col m12">
                       <div class="col m6">
                           <label>Ссылка</label>
                           <input type="url" name="url">
                       </div>

                       <div class="col m6">
                           <div class="file-field input-field">
                               <div class="btn">
                                   <span>Баннер</span>
                                   <input type="file" name="image" accept="image/*" required="">
                               </div>
                               <div class="file-path-wrapper">
                                   <input class="file-path validate" type="text">
                               </div>
                           </div>
                       </div>
                       <button type="submit" class="btn btn-success right">Загрузить</button>
                   </div>
               </form>

               <div class="row">
                   <? foreach ($banners as $banner): ?>
                       <div class="col m6">
                           <div class="card">
                               <div class="card-image waves-effect waves-block waves-light">
                                   <img class="activator" height="320" src="/content/banners/<?= $banner->image ?>">
                               </div>
                               <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><i
                                        class="mdi-navigation-close right"></i></span>

                                   <form action="/administrator/banner/<?= $banner->id ?>" method="post">
                                       <label>Ссылка</label>
                                       <input type="url" name="url" value="<?= $banner->url ?>">
                                       <button type="submit" class="btn btn-success right">Сохранить</button>

                                   </form>

                                   <a class="btn red left" href="/administrator/banner/<?= $banner->id ?>">Удалить баннер</a>
                               </div>

                           </div>
                       </div>
                   <? endforeach ?>
               </div>
           </div>
        <?endif?>
    </div>

    <?if($form->instance->id):?>
        <div class="col m4">
            <div class="card card-panel">
                <h3 class="card-title black-text">Натройки поста</h3>
                <a href="/view/<?= $form->instance->alias ?>" target="_blank" class="btn blue right"><i
                        class="mdi-image-remove-red-eye"></i></a>

                <form method="post" action="/administrator/post/<?= $post->instance->id ?>" enctype="multipart/form-data">
                    <div class="col m12">
                        <?= $post->field('title') ?>
                        <?= $post->field('link') ?>
                        <?= $post->field('vk') ?>
                        <?= $post->field('fb') ?>
                        <?= $post->field('ok') ?>
                        <?= $post->field('tw') ?>

                        <label>Изображения</label>
                        <? if ($post->instance->image): ?>
                            <img src="/content/posts/<?= $post->instance->image ?>" class="responsive-img">
                        <? endif ?>
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Изображение</span>
                                <input type="file" name="image" accept="image/*">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <?= $post->field('text', ['class' => 'materialize-textarea']) ?>
                    </div>

                    <div class="col m12">
                        <button type="submit" class="btn btn-success right">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    <?endif?>

</div>
