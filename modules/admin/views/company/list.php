<? $this->title = 'Компании | Административная панель'; ?>

<a href="/administrator/companies/add" class="btn-floating btn-large waves-effect red"
    style="position: fixed; top: 80%; left: 90%;"><i class="mdi-action-note-add"></i></a>

<div class="container">
    <table class="table bordered striped ">
        <tr>
            <td>#</td>
            <td>Название компании</td>
            <td>Изображение</td>
            <td>Окончание контракта</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <?foreach($companies as $company):?>
            <tr>
                <td><?=$company->id?></td>
                <td><?=$company->name?></td>
                <td><?if ($company->image):?><img src="/content/companies/<?=$company->image?>" width="50"><?endif?></td>
                <td><?=$company->date_end?></td>
                <td><a target="_blank" href="/adword/<?=$company->alias?>"><i class="mdi-image-remove-red-eye"></i></a> </td>
                <td><a href="/administrator/companies/edit/<?=$company->id?>"><i class="mdi-image-edit"></i></a> </td>
                <td><a href="/administrator/companies/remove/<?=$company->id?>" class="delete"><i class="mdi-content-remove-circle"></i></a> </td>
            </tr>
        <?endforeach;?>
    </table>

    <script>
        $('.delete').click(function(e) {
            e.preventDefault();

            Materialize.toast('<a href="'+$(this).attr('href')+'">Удалить компанию</a>');
        })
    </script>
</div>

