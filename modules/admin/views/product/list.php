<? $this->title = "Товари"; ?>

<div class="container">

    <a href="/administrator/recommendations/products/add" class="btn-floating btn-large waves-effect red"
       style="position: fixed; bottom: 20%; right: 10%;"><i class="mdi-action-note-add"></i></a>

    <table class="table bordered striped highlight">
        <tr>
            <td>#</td>
            <td>Товар</td>
            <td>Цена (грн)</td>
            <td>Количество</td>
            <td>Категория</td>
            <td>Изображения</td>
            <td></td>
            <td></td>
        </tr>

        <?foreach($products as $product):?>
            <tr>
                <td><?=$product->id?></td>
                <td><?=$product->title?></td>
                <td><?=$product->price?></td>
                <td><?=$product->qty?></td>
                <td><?=$product->category?></td>
                <td><?if ($product->image):?><img src="/content/products/<?=$product->image?>" width="50"><?endif?></td>
                <td><a href="/administrator/recommendations/products/edit/<?=$product->id?>"><i class="mdi-image-edit"></i></a> </td>
                <td><a href="/administrator/recommendations/products/remove/<?=$product->id?>" class="delete"><i class="mdi-content-remove-circle"></i></a> </td>
            </tr>
        <?endforeach;?>
    </table>

    <script>
        $('.delete').click(function(e) {
            e.preventDefault();

            Materialize.toast('<a href="'+$(this).attr('href')+'">Удалить продукт</a>');
        })
    </script>
</div>

