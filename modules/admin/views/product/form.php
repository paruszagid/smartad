<?
/**
 * @var \modules\admin\forms\CompanyForm $form
 */

$this->title = ($form->instance->title ? $form->instance->title : 'Новый товар');
?>
<script src="/assets/libs/underscore-min.js"></script>

<div class="container">
    <div>
        <div class="row">
            <h3 class="card-title black-text center"><?= $this->title ?></h3>
            <button type="submit" class="btn green right" form="product_form">Сохранить</button>

            <form method="post" id="product_form" action="<?= ($form->instance->id ?
                '/administrator/recommendations/products/update/' . $form->instance->id :
                '/administrator/recommendations/products/save/') ?>" enctype="multipart/form-data">
                <div class="col m8">
                    <?= $form->field('title') ?>
                    <?= $form->field('description', ['class' => 'materialize-textarea']) ?>
                </div>

                <div class="col m4">
                    <?= $form->field('qty') ?>
                    <?= $form->field('price') ?>
                    <?= $form->field('category') ?>

                    <label>Картинка</label>
                    <? if ($form->instance->image): ?>
                        <img src="/content/products/<?= $form->instance->image ?>" style="max-width: 100%;">
                    <? endif ?>
                    <input type="file" name="image" accept="image/*">
                </div>
            </form>
        </div>
    </div>
</div>
