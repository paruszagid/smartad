<? $this->title = "Рекламма" ?>


<a href="/administrator/adword/add" class="btn-floating btn-large waves-effect red"
   style="position: fixed; top: 80%; left: 90%;"><i class="mdi-action-note-add"></i></a>

<a href="/administrator/adword/media" class="btn green right" style="margin: 5px;"><i class="mdi-action-view-list"></i></a>

<div class="container">
    <table class="table bordered striped highlight">
        <tr>
            <td>#</td>
            <td>Название компании</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <? foreach ($adwords as $adword): ?>
            <tr>
                <td><?= $adword->id ?></td>
                <td><?= $adword->title ?></td>
                <td>
                    <? if ($adword->image): ?>
                        <i class="mdi-image-image"></i>
                    <? endif ?>
                </td>
                <td>
                    <? if ($adword->audio): ?>
                        <i class="mdi-image-audiotrack"></i>
                    <? endif ?>
                </td>
                <td><a href="/administrator/adword/edit/<?= $adword->id ?>"><i class="mdi-image-edit"></i></a></td>
                <td><a href="/administrator/adword/remove/<?= $adword->id ?>" class="delete"><i
                            class="mdi-content-remove-circle"></i></a></td>
            </tr>
        <? endforeach; ?>
    </table>
</div>

<script>
    $('.delete').click(function (e) {
        e.preventDefault();

        Materialize.toast('<a href="' + $(this).attr('href') + '">Удалить рекламму</a>');
    })
</script>


