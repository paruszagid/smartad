<? $this->title = 'Медиаплан для рекламмы' ?>
<link rel="stylesheet" href="/assets/fullcalendar/dist/fullcalendar.min.css">

<script src="/assets/angular/angular.js"></script>
<script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
<script src="/assets/moment/moment.js"></script>
<script src="/assets/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/assets/fullcalendar/dist/gcal.js"></script>
<script src="/assets/angular-ui-calendar/src/calendar.js"></script>
<script src="/assets/moment/moment.js"></script>

<div ng-app="adwordMedia">
    <section id="directives-calendar" ng-controller="CalendarCtrl">
        <div class="well">
            <div class="row">
                <div class="col m12">
                    <div class="row">
                        <button class="btn btn-success" ng-click="changeView('agendaDay', 'myCalendar1')"><i
                                class="mdi-action-view-day"></i> День
                        </button>
                        <button class="btn btn-success" ng-click="changeView('agendaWeek', 'myCalendar1')"><i
                                class="mdi-action-view-week"></i> Неделя
                        </button>
                        <button class="btn btn-success" ng-click="changeView('month', 'myCalendar1')"><i
                                class="mdi-action-view-module"></i> Месяц
                        </button>
                    </div>


                    <button ng-click="addMedia();" class="btn blue right">Добавить рекламму к медиаплану</button>

                    <div class="calendar" ng-model="eventSources" calendar="myCalendar1"
                         ui-calendar="uiConfig.calendar"></div>
                </div>
            </div>
        </div>

        <!-- Modal Structure -->
        <div id="editEvent" class="modal">
            <div class="modal-content">
                <button class="btn red right modal-action modal-close" ng-click="deleteEvent(event_id);">Удалить медиаплан</button>
                {{event_title}}
                <p>
                    <label>Начало показа</label>
                    <input type="datetime-local" ng-model="event_date_begin">

                    <label>Конец показа</label>
                    <input type="datetime-local" ng-model="event_date_end">
                </p>
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Отмена</a>
                <a ng-click="saveEvent();" href="#!"
                   class=" modal-action modal-close waves-effect waves-green btn-flat">Сохранить</a>
            </div>
        </div>

        <!-- Modal Structure -->
        <div id="addMedia" class="modal">
            <div class="modal-content">
                <form name="addAdwordForm" id="addAdwordF" ng-submit="createEvent();">
                    <label>Рекламма</label>
                    <select class="browser-default" ng-model="add_event_adword" required>
                        <option ng-repeat="adword in adwords" value="{{adword.id}}">{{adword.title}}</option>
                    </select>

                    <label>Начало показа</label>
                    <input type="datetime-local" ng-model="add_event_begin" required>

                    <label>Конец показа</label>
                    <input type="datetime-local" ng-model="add_event_end" required>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Отмена</a>
                <button type="submit" form="addAdwordF" ng-show="addAdwordForm.$valid"
                        class=" modal-action modal-close waves-effect waves-green btn-flat">Добавить
                </button>
            </div>
        </div>
    </section>
</div>


<script>
    var app = angular.module('adwordMedia', ['ui.bootstrap', 'ui.calendar'], function ($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
        $httpProvider.defaults.transformRequest = [
            function (data) {
                var param = function (obj) {
                    var query = '';
                    var name, value, fullSubName, subName, subValue, innerObj, i;

                    for (name in obj) {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value instanceof Object) {
                            for (subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '=' +
                                encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]' ?
                    param(data) : data;
            }
        ];
    });

    app.controller('CalendarCtrl',
        function ($scope, $compile, $timeout, uiCalendarConfig, $http) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();


            $scope.events = [];


            $scope.setEvents = function (data) {
                $scope.events.length = 0;
                angular.forEach(data, function (item) {
                    $scope.events.push({
                        id: item.id,
                        title: item.title,
                        start: new Date(item.date_begin * 1000),
                        end: new Date(item.date_end * 1000)
                    });

                });
            };

            $http.get('/administrator/adword/events')
                .success($scope.setEvents);

            /* alert on eventClick */
            $scope.alertOnEventClick = function (date, jsEvent, view) {
                $scope.event_title = date.title;
                $scope.event_id = date.id;
                $scope.event_date_begin = new Date(date.start);
                $scope.event_date_end = new Date(date.end);
                $('#editEvent').openModal();
            };

            $scope.addMedia = function () {
                $http.get('/administrator/adword/adwords')
                    .success(function (data) {
                        $scope.add_event_adword = null;
                        $scope.adwords = data;
                        $('#addMedia').openModal();
                    });
            };

            $scope.saveEvent = function () {
                $http.post('/administrator/adword/change_event/' + $scope.event_id, {
                    date_begin: "" + new Date($scope.event_date_begin),
                    date_end: "" + new Date($scope.event_date_end)
                }).success($scope.setEvents);
            };

            $scope.createEvent = function () {
                $http.post('/administrator/adword/create_event', {
                    adword_id: $scope.add_event_adword,
                    date_begin: "" + new Date($scope.add_event_begin),
                    date_end: "" + new Date($scope.add_event_end)
                }).success($scope.setEvents);
            };

            $scope.deleteEvent = function (id) {
                if (confirm('Удалить "' + $scope.event_title + '" ?')) {
                    $http.delete('/administrator/adword/delete_event/' + id)
                        .success($scope.setEvents);
                }
            };
            /* alert on Drop */
            $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
                $http.post('/administrator/adword/delta_event/' + arguments[0].id, {
                    delta: parseInt("" + delta) / 1000
                });
            };
            /* alert on Resize */
            $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
                $http.post('/administrator/adword/delta_event/' + arguments[0].id, {
                    delta: parseInt("" + delta) / 1000
                });
            };
            /* add and removes an event source of choice */
            $scope.addRemoveEventSource = function (sources, source) {
                var canAdd = 0;
                angular.forEach(sources, function (value, key) {
                    if (sources[key] === source) {
                        sources.splice(key, 1);
                        canAdd = 1;
                    }
                });
                if (canAdd === 0) {
                    sources.push(source);
                }
            };
            /* add custom event*/
            $scope.addEvent = function () {
                $scope.events.push({
                    title: 'Open Sesame',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    className: ['openSesame']
                });
            };
            /* remove event */
            $scope.remove = function (index) {
                $scope.events.splice(index, 1);
            };
            /* Change View */
            $scope.changeView = function (view, calendar) {
                uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
            };
            /* Change View */
            $scope.renderCalender = function (calendar) {
                $timeout(function () {
                    if (uiCalendarConfig.calendars[calendar]) {
                        uiCalendarConfig.calendars[calendar].fullCalendar('render');
                    }
                });
            };
            /* Render Tooltip */
            $scope.eventRender = function (event, element, view) {
                element.attr({
                    'tooltip': event.title,
                    'tooltip-append-to-body': true
                });
                $compile(element)($scope);
            };
            /* config object */
            $scope.uiConfig = {
                calendar: {
                    editable: true,
                    header: {
                        left: '',
                        center: 'title',
                        right: ''
                    },
                    eventClick: $scope.alertOnEventClick,
                    eventDrop: $scope.alertOnDrop,
                    eventResize: $scope.alertOnResize,
                    eventRender: $scope.eventRender,

                    timeFormat: 'H(:mm)'
                }
            };

            /* event sources array*/
            $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
            $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];

            $scope.uiConfig.calendar.dayNames = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
            $scope.uiConfig.calendar.dayNamesShort = ["Вос", "Пон", "Вт", "Ср", "Чт", "Пт", "Сб"];
        });
    /* EOF */
</script>