<?
/**
 * @var \modules\admin\forms\CompanyForm $form
 */

$this->title = ($form->instance->id ? $form->instance->title : 'Новая рекламма');
?>


<div class="row">
    <div class="col m8">
        <div class="card card-panel">
            <form method="post" action="<?= ($form->instance->id ?
                '/administrator/adword/update/' . $form->instance->id :
                '/administrator/adword/save/') ?>" enctype="multipart/form-data">

                <h3 class="card-title black-text left"><?= $this->title ?></h3>

                <button class="btn blue right"><i class="mdi-content-save"></i> Сохранить</button>

                <div class="col m12">
                    <?= $form->field('title') ?>
                    <?= $form->field('link') ?>
                </div>

                <div class="col m6">
                    <? if ($form->instance->id and $form->instance->image): ?>
                        <div>
                            <img src="/content/adwords/images/<?= $form->instance->image ?>" class="responsive-img materialboxed">
                            <a class="btn right red" style="position: relative; bottom: 10%;" href="/administrator/adword/remove_image/<?=$form->instance->id?>">
                                <i class="mdi-content-remove-circle"></i>
                            </a>
                        </div>
                    <? endif ?>

                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Изображения</span>
                            <input type="file" name="image" accept="image/*">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>

                    <? if ($form->instance->id and $form->instance->audio): ?>
                        <audio class="blue" src="/content/adwords/audios/<?= $form->instance->audio ?>"
                               controls></audio>

                        <a class="btn right red" style="position: relative; bottom: 10%;" href="/administrator/adword/remove_audio/<?=$form->instance->id?>">
                            <i class="mdi-content-remove-circle"></i>
                        </a>
                    <? endif ?>
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Аудио</span>
                            <input type="file" name="audio" accept="audio/*">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div class="col m6">
                    <?= $form->field('text', ['class' => 'materialize-textarea']) ?>
                    <?= $form->field('video', ['class' => 'materialize-textarea']) ?>
                </div>
            </form>
        </div>
    </div>

    <? if ($form->instance->id): ?>
        <div class="col m4">
            <div class="card card-panel">
                <h3 class="card-title black-text left">Медиаплан</h3>

                <button class="btn green right" type="submit" form="media_form"><i class="mdi-content-save"></i> Добавить
                </button>
                <div class="col m12">
                    <form method="post" id="media_form"
                          action="/administrator/adword/add_media/<?= $form->instance->id ?>">
                        <p>Начало показа</p>
                        <input type="datetime-local" name="date_begin" required>

                        <p>Окончание показа</p>
                        <input type="datetime-local" name="date_end" required>
                    </form>
                </div>

                <div class="col m12">
                    <p>Сейчас : <?=date("d-m-Y H:i")?></p>
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Дата начала</td>
                            <td>Дата окончания</td>
                            <td></td>
                        </tr>
                        </thead>

                        <tbody>
                        <? foreach ($media_plan as $media): ?>
                            <tr>
                                <td><?=date("d-m-Y H:i", $media->date_begin)?></td>
                                <td><?=date("d-m-Y H:i", $media->date_end)?></td>
                                <td><a href="/administrator/adword/remove_media/<?= $media->id ?>">
                                        <i class="mdi-content-remove-circle"></i>
                                    </a></td>
                            </tr>
                        <? endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card card-panel">
                <h3 class="card-title black-text">Привязка к компаниям</h3>

                <form method="post" action="/administrator/adword/add_company/<?=$form->instance->id?>">
                    <label>Комания</label>
                    <select class="browser-default" name="company_id" required>
                        <?foreach($companies as $company):?>
                        <option value="<?=$company->id?>" ><?=$company->name?></option>
                        <?endforeach?>
                    </select>

                    <label>Что делать</label>
                    <select class="browser-default" name="access">
                        <option value="1">Показывать</option>
                        <option value="0">Не показывать</option>
                    </select>

                    <button class="btn right">Добавить</button>
                </form>

                <div class="col m12">
                    <ul class="collection">
                        <?foreach($adword_company as $ac):?>
                        <li class="collection-item">
                           <table class="table">
                               <tr>
                                   <td><?=$ac->name?></td>
                                   <td><?=($ac->access == 0 ? 'Не показывать': 'Показывать')?></td>
                                   <td><a href="/administrator/adword/remove_company/<?=$ac->id?>" class="btn-flat"><i class="mdi-content-remove-circle"></i></a> </td>
                               </tr>
                           </table>
                        </li>
                        <?endforeach?>
                    </ul>
                </div>
            </div>
        </div>
    <? endif ?>
</div>