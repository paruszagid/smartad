<? $this->title = \app\sitebuilder\SiteBuilder::$app->siteName . ' | Административная панель'; ?>
<div class="row">
    <div class="col m6">
        <div class="card">
            <h3 class="card-title black-text center">Новые компании</h3>
            <div class="card-content">
                <ul class="collection">
                    <?foreach($companies as $company):?>
                    <li class="collection-item">
                        <a href="/administrator/companies/edit/<?=$company->id?>">
                            <?=$company->name?>
                        </a>
                    </li>
                    <?endforeach?>
                </ul>
            </div>

            <div class="card-action">
                <a href="/administrator/companies">Полный список <i class="mdi-navigation-chevron-right"></i></a>
            </div>
        </div>
    </div>

    <div class="col m6">
        <div class="card">
            <h3 class="card-title black-text center">Рекламма которая сейчас показывается</h3>
            <div class="card-content">
                <ul class="collection">
                    <?foreach($adwords as $adword):?>
                        <li class="collection-item">
                            <a href="/administrator/adword/edit/<?=$adword->id?>">
                                <?=$adword->title?>
                            </a>
                        </li>
                    <?endforeach?>
                </ul>
            </div>

            <div class="card-action">
                <a href="/administrator/adword">Полный список <i class="mdi-navigation-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>

