<?
    /**
     * @var \modules\admin\forms\CompanyForm $form
     */

$this->title = ($form->instance->title ? $form->instance->title : 'Новая категория');
?>


<div class="container">
    <div class="row">
        <form method="post" action="<?=($form->instance->title ?
            '/administrator/recommendations/categories/update/'. $form->instance->id :
            '/administrator/recommendations/categories/save/')?>" enctype="multipart/form-data">
            <div class="col m9">
                <?=$form->field('title')?>
                <?=$form->field('description', ['class' => 'materialize-textarea'])?>
            </div>

            <div class="col m3">
                <button type="submit" class="btn green">Зберегти</button>
            </div>
        </form>
    </div>
</div>
