<? $this->title = "Категории"; ?>

<a href="/administrator/recommendations/categories/add" class="btn-floating btn-large waves-effect red"
    style="position: fixed; top: 80%; left: 90%;"><i class="mdi-action-note-add"></i></a>

<div class="container">
    <table class="table bordered striped highlight">
        <tr>
            <td>#</td>
            <td>Категория</td>
            <td></td>
            <td></td>
        </tr>

        <?foreach($categories as $category):?>
            <tr>
                <td><?=$category->id?></td>
                <td><?=$category->title?></td>
                <td><a href="/administrator/recommendations/categories/edit/<?=$category->id?>"><i class="mdi-image-edit"></i></a> </td>
                <td><a href="/administrator/recommendations/categories/remove/<?=$category->id?>" class="delete"><i class="mdi-content-remove-circle"></i></a> </td>
            </tr>
        <?endforeach;?>
    </table>

    <script>
        $('.delete').click(function(e) {
            e.preventDefault();

            Materialize.toast('<a href="'+$(this).attr('href')+'">Удалить категорию</a>');
        })
    </script>
</div>

