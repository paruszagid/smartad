<?
use app\sitebuilder\SiteBuilder;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Вход в систему</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/materialize/css/materialize.min.css">
    <script src="/assets/jquery/jquery.min.js"></script>
    <script src="/assets/materialize/js/materialize.min.js"></script>
</head>
<body style="background: url(/assets/landing/images/banner.jpg)">

    <div class="card" style="width: 400px; margin: 10% auto;">
        <form method="POST" action="/administrator/sign_in">
        <h3 class="card-title" style="color: inherit; text-align: center;">
            <?=SiteBuilder::$app->siteName?>
        </h3>

        <div class="card-content">

               <label>Логин</label>
               <input type="text" name="login" required>

               <label>Пароль</label>
               <input type="password" name="password" required>
        </div>

        <div class="card-action">
            <button class="btn right waves-effect blue" type="submit">Вход</button>
        </div>

        </form>
    </div>
</body>
</html>