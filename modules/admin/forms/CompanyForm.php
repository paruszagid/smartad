<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 22:44
 */

namespace modules\admin\forms;


use app\sitebuilder\ModelForm;

class CompanyForm extends ModelForm
{
    public $model = 'models\Company';
}