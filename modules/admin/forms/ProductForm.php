<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 15.10.15
 * Time: 21:23
 */

namespace modules\admin\forms;


use app\sitebuilder\ModelForm;

class ProductForm extends ModelForm
{
    public $model = 'models\Product';
}