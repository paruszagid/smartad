<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 15.10.15
 * Time: 21:19
 */

namespace modules\admin\forms;


use app\sitebuilder\ModelForm;

class CategoryForm extends ModelForm
{
    public $model = 'models\Category';
}