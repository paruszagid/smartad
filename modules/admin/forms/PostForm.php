<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 15.10.15
 * Time: 21:46
 */

namespace modules\admin\forms;


use app\sitebuilder\ModelForm;

class PostForm extends ModelForm
{
    public $model = 'models\Post';
}