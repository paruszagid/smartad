<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 09.10.15
 * Time: 20:42
 */

namespace modules\admin\forms;


use app\sitebuilder\ModelForm;

class AdwordForm extends ModelForm
{
    public $model = 'models\Adword';
}