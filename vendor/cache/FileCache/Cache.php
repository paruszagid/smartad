<?php
namespace app\cache\FileCache;


use app\sitebuilder\Component;

/**
 * Клас для кешування даних в файловій системі
 * Class Cache
 * @package app\cache\FileCache
 * @author Novoseletskiy Serhiy <novoseletskiyserhiy@gmail.com>
 *
 * @property string $cachepath шлях до папки де будуть зберігатись дані
 * @property string $extension розширення файлу з даними
 */
class Cache implements Component {
    public $cachepath = '/cache';
    public $extension = '.cache';

    function init() {
        if (!is_dir($this->cachepath)) {
            mkdir($this->cachepath);
        }
    }

    /**
     * Медот для збереження даних в файлову систему
     * @param string $key
     * @param $data
     * @param null|int $time якщо null то дані будуть зберігатись до тих пір доки їх вручну не видалять
     * @return $this
     */
    function set($key, $data, $time = null) {
        $storeData = array(
            'data' => $data,
            'time' => $time + time()
        );

        $fileName = md5($key).$this->extension;
        file_put_contents($this->cachepath . $fileName ,serialize($storeData));
        return $this;
    }

    /**
     * Метод для отримання даних з файлової системи
     * @param $key
     * @return mixed
     */
    function get($key) {
        $fileName = md5($key).$this->extension;

        if (file_exists($this->cachepath . $fileName)) {
            $cacheData = file_get_contents($this->cachepath . $fileName);
            $cacheData = unserialize($cacheData);

            if (!$cacheData['time'])
                return $cacheData['data'];

            if ($cacheData['time'] >= time())
                return $cacheData['data'];
        }

        return null;
    }

    /**
     * Метод для видалення даних
     * @param string $key
     */
    function remove($key) {
        $fileName = md5($key).$this->extension;

        if (file_exists($this->cachepath . $fileName)) {
            unlink($this->cachepath . $fileName);
        }
    }
}
