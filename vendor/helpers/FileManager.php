<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 13.09.15
 * Time: 20:16
 */

namespace app\helpers;


class FileManager
{
    public static function deleteFile($fileName)
    {
        if (file_exists($fileName))
            unlink($fileName);
    }

    public static function uploadImage($file, $uploadPath)
    {
        $_FILES[$file]['type'] = strtolower($_FILES['image']['type']);

        if ($_FILES[$file]['type'] == 'image/png'
            || $_FILES[$file]['type'] == 'image/jpg'
            || $_FILES[$file]['type'] == 'image/gif'
            || $_FILES[$file]['type'] == 'image/jpeg'
            || $_FILES[$file]['type'] == 'image/pjpeg'
        ) {
            $arr = explode('.', $_FILES[$file]['name']);
            $image = md5(microtime()) . '.' . $arr[count($arr) - 1];;
            move_uploaded_file($_FILES[$file]['tmp_name'], $uploadPath . $image);
            return $image;
        }

        return null;
    }

    public static function uploadFile($file, $uploadPath)
    {
        if ($_FILES[$file]['name']) {
            $arr = explode('.', $_FILES[$file]['name']);

            $f = md5($_FILES[$file]['name']) . '.' . $arr[count($arr) - 1];
            move_uploaded_file($_FILES[$file]['tmp_name'], $uploadPath . $f);
            return $f;
        }

        return null;
    }
}