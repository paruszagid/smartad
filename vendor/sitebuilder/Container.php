<?php

namespace app\sitebuilder;

/**
 * В даному класі будуть зберігатись дані які будуть доступні з будь якого місця програми через статичну зміну
 * SiteBuilder::$app
 *
 * Приклад:
 *  Sitebuilder::$app->cache->get('test');
 *
 * або напряму через статичний метод get
 *
 * Приклад:
 *  (Container::get('cache'))->get('test');
 *
 * Class Container
 * @package app\sitebuilder
 * @author Novoseletskiy Serhiy <novoseletskiyserhiy@gmail.com>
 */
class Container
{
    public static $container = [];

    public static function add($key, $value)
    {
        self::$container[$key] = $value;
    }

    public static function get($key)
    {
        if (isset(self::$container[$key]))
            return self::$container[$key];

        return null;
    }

}