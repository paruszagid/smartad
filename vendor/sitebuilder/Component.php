<?php

namespace app\sitebuilder;

/**
 * Базовий інтерфейс для всіх компонентів
 * Interface Component
 * @package app\sitebuilder
 * @author Novoseletskiy Serhiy <novoseletskiyserhiy@gmail.com>
 */
interface Component
{
    /**
     * Метод для ініціалізації початковий даних для певного компонента
     * @return mixed
     */
    public function init();
}