<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 07.08.15
 * Time: 12:23
 */

namespace app\sbuser;


use app\sitebuilder\Component;
use app\sitebuilder\Container;

class Module implements Component {
    public $user = 'app\sbuser\User';
    public $redirectTo = '/login';
    public $excluded;
    public $key = 'user';

    function init() {
        $this->user = new $this->user;

        if (!$this->user->isAuth() and !in_array($_SERVER['REQUEST_URI'] , $this->excluded)) {
            header("Location: ". $this->redirectTo);
        } else {
            Container::add($this->key, call_user_func([$this->user, 'getByPk'], $_SESSION['user_id']));
        }
    }
}