<?php
namespace app\vk;

interface IVkPhpSdk
{
	/**
	 * Get OAuth 2.0 access token.
	 * 
	 * @return string
	 */
	public function getAccessToken();
	
	/**
	 * Set OAuth 2.0 access token. 
	 * 
	 * @param string $accessToken with access token we can make calls to secure API
	 */
	public function setAccessToken($accessToken);

	/**
	 * Get user id.
	 * 
	 * @return string
	 */
	public function getUserId();
	
	/**
	 * Set user id.
	 * 
	 * @return string
	 */
	public function setUserId($userId);

	/**
	 * Makes a call to VK API.
	 *
	 * @param string $method The API method name
	 * @param array $params The API call parameters
	 * 
	 * @return array decoded response
	 * 
	 * @throws VkApiException
	 */
	public function api($method, array $params = null);
}