<?php
namespace app\vk;

interface IOauth2Proxy
{
	/**
	 * Constructor.
	 * 
	 * @param string $clientId id of the client application
	 * @param string $clientSecret application secret key
 	 * @param string $accessTokenUrl access token url
	 * @param string $dialogUrl dialog url
	 * @param string $responseType response type (for example: code)
	 * @param string $redirectUri redirect uri
	 * @param string $scope access scope (for example: friends,video,offline)
	 */
	public function __construct($clientId, $clientSecret, $accessTokenUrl, $dialogUrl, $responseType, $redirectUri = null, $scope = null);
	
	/**
	 * Authorize client.
	 */	
	public function authorize();
	
	/**
	 * Get access token.
	 * 
	 * @return string
	 */
	public function getAccessToken();
	
	/**
	 * Get expires time.
	 * 
	 * @return string
	 */
	public function getExpiresIn();
	
	/**
	 * Get user id.
	 * 
	 * @return string
	 */
	public function getUserId();
}