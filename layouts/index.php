<?
use app\widgets\bootstrap\Nav;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $this->title ?></title>

    <? $this->getCssFiles(); ?>


</head>
<body>
<div class="navbar-fixed thin">

    <ul id="docs" class="dropdown-content">
        <li><a href="/docs/routing.html">Routing</a></li>
    </ul>

    <ul id="m-docs" class="dropdown-content">
        <li><a href="/docs/routing.html">Routing</a></li>
    </ul>


    <nav>
        <div class="nav-wrapper red">
            <a href="/" class="brand-logo"><?=\app\sitebuilder\SiteBuilder::$app->siteName?></a>
    </nav>
</div>


<div class="container">
    <div class="row">
        <?= $this->content?>
    </div>
</div>


<? $this->getJsFiles() ?>
<script type="text/javascript">SyntaxHighlighter.all();</script>
</body>
</html>