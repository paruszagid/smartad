<!DOCTYPE html>
<html>
<head>
    <title><?=$this->title?></title>
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/jquery/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <link href="/assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Custom Theme files -->

    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--Animation-->
    <script src="/assets/landing/js/wow.min.js"></script>
    <link href="/assets/landing/css/animate.css" rel='stylesheet' type='text/css' />
    <script>
        new WOW().init();
    </script>
    <script type="text/javascript" src="/assets/landing/js/move-top.js"></script>
    <script type="text/javascript" src="/assets/landing/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
    <script src="/assets/slider/js/wpts_slider_multiple.js"></script>
    <link rel="stylesheet" href="/assets/slider/css/slider_multiple.css">
</head>
<body>
<div class="content">
    <?=$this->content?>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>