<?
use app\sitebuilder\SiteBuilder;
?>
<!DOCTYPE html>
<html>
<head>
    <title><?=$this->title?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/materialize/css/materialize.min.css">
    <script src="/assets/jquery/jquery.min.js"></script>
    <script src="/assets/materialize/js/materialize.js"></script>
</head>
<body>
<div class="wrapper">
    <div>
        <?=$this->content?>
    </div>

    <footer class="page-footer green">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">SmartAd</h5>
                    <p class="grey-text text-lighten-4">
                        <i class="mdi-communication-phone"></i> +38 (050) 434 0 333, +38 (067) 372 2 767
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2015 SmartAd
                <a class="grey-text text-lighten-4 right" href="mailto:info@sisoftware.biz">info@sisoftware.biz</a>
            </div>
        </div>
    </footer>
</div>

</body>
</html>