<?php

return [
    'models' =>  __DIR__. '/../models/',
    'modules' =>  __DIR__. '/../modules',
    'controllers' =>  __DIR__. '/../controllers'
];