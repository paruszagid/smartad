<?php ;


return [
    'siteName' => 'SmartAd',
    'language' => 'uk',
    'layoutPath' => __DIR__ . '/../layouts/',
    'viewsPath' => __DIR__ . '/../views/',
    'translatePath' => __DIR__ . '/../translates/',
    'layout' => 'index',
    'companyImagePath' => __DIR__ . '/../content/companies/',
    'companyBannersPath' => __DIR__ . '/../content/banners/',
    'postImagePath' => __DIR__ . '/../content/posts/',
    'postAudioPath' => __DIR__ . '/../content/posts/audio/',
    'adwordImagePath' => __DIR__ . '/../content/adwords/images/',
    'adwordAudioPath' => __DIR__ . '/../content/adwords/audios/',
    'route' => require_once(__DIR__ . '/route.php'),
    'components' => [
        [
            'alias' => 'request',
            'class' => 'app\sitebuilder\Request'
        ],
        [
            'alias' => 'db',
            'class' => 'app\sitebuilder\Database',
            'options' => [
                'host' => 'localhost',
                'user' => 'root',
                'password' => '',
                'database' => 'smartad'
            ]
        ], [
            'alias' => 'cache',
            'class' => 'app\cache\FileCache\Cache',
            'options' => [
                'cachepath' => __DIR__ . '/../cache/'
            ]
        ]
    ],
    'assets' => [
        'js' => [
            '/assets/jquery/jquery.min.js',
            '/assets/materialize/js/materialize.min.js',
            '/assets/syntaxhighlighter/scripts/shCore.js',
            '/assets/syntaxhighlighter/scripts/shBrushPhp.js',
            '/assets/syntaxhighlighter/scripts/shBrushXml.js'
        ],
        'css' => [
            '/assets/materialize/css/materialize.min.css',
            '/assets/syntaxhighlighter/styles/shCoreDefault.css'
        ]
    ]];