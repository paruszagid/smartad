<?php
use app\vk\Oauth2Proxy;
use app\vk\Vk;
use app\vk\VkPhpSdk;

return
    [
        '^/$' => [
            'callback' => 'controllers\SiteController::main_pageAction'
        ],

        '^/get_all_adwords/$' => [
            'callback' => 'controllers\SiteController::get_all_adwordsAction'
        ],

        '^/view/(?P<id>\w+)' => [
            'callback' => 'controllers\SiteController::viewAction'
        ],

        '^/adword/$' => [
            'callback' => function() {
                header("Location: /");
            }
        ],

        '^/adword/(?P<alias>\w+)/$' => [
            'callback' => 'controllers\SiteController::adwordAction'
        ],

        '^/registration/(?P<id>\d+)' => [
            'template' => 'landing/home',
            'layout' => 'landing'
        ],

        '^/validation' => [
            'POST' => [
                'callback' => 'controllers\SiteController::validationAction'
            ]
        ],

        '^/administrator/(.*)' => [
            'middleware' => [
                'app\sbuser\Module' => [
                    'redirectTo' => '/administrator/login',
                    'user' => 'app\sbuser\User',
                    'excluded' => [
                        '/administrator/login',
                        '/administrator/sign_in'
                    ],
                    'key' => 'admin'
                ]
            ],
            'module' => 'modules\admin\AdminModule'
        ],

        '^/(?P<alias>\w+)/$' => [
            'callback' => 'controllers\SiteController::companyAction'
        ]
    ];