<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 12.09.15
 * Time: 21:44
 */

namespace models;


use app\sitebuilder\Model;

class Post extends Model
{
    public $fields = [
        'id' => [
            'type' => 'INT'
        ],


        'title' => [
            'type' => 'VARCHAR',
            'length' => 200,
            'label' => 'Названия поста',
            'required' => false
        ],

        'text' => [
            'type' => 'TEXT',
            'label' => 'Текст',
            'required' => false
        ],

        'link' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка',
            'required' => false
        ],

        'image' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка',
            'required' => true
        ],

        'company_id' => [
            'type' => 'FK',
            'class' => 'models\Company',
        ],

        'vk' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка на пост в Vk',
        ],

        'fb' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка на пост в Facebook',
        ],

        'ok' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка на пост в Odnoklassniki',
        ],

        'tw' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Ссылка на пост в Twitter',
        ],
    ];

    public function tableName()
    {
        return 'posts';
    }
}