<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 12.09.15
 * Time: 23:28
 */

namespace models;


use app\sitebuilder\Model;

class PostHistory extends Model
{
    public $fields = [
        'id' => [],
        'number' => [],
        'company_id' => [],
        'time' => [],
    ];

    public function tableName()
    {
        return 'posts_history';
    }
}