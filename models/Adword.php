<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 09.10.15
 * Time: 20:36
 */

namespace models;


use app\sitebuilder\Model;

class Adword extends Model
{
    public $fields = [
        'id' => [
            'type' => 'INT'
        ],

        'title' => [
            'type' => 'VARCHAR',
            'length' => 200,
            'required' => true,
            'label' => 'Название рекламмы'
        ],

        'link' => [
            'type' => 'VARCHAR',
            'length' => 200,
             'label' => 'Ссылка'
        ],

        'text' => [
            'type' => 'TEXT',
            'label' => 'Текст'
        ],

        'image' => [
            'type' => 'VARCHAR',
            'length' => 50,
            'label' => 'Картинка',
        ],

        'video' => [
            'type' => 'TEXT',
            'label' => 'Код для видео'
        ],

        'audio' => [
            'type' => 'VARCHAR',
            'length' => 50
        ]
    ];

    public function tableName()
    {
        return 'adwords';
    }
}