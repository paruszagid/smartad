<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 05.09.15
 * Time: 15:38
 */

namespace models;


use app\sitebuilder\Model;

class Product extends Model
{
    public $fields = [
        'id' => [
            'type' => 'INT'
        ],

        'title' => [
            'type' => 'VARCHAR',
            'label' => 'Название товара',
            'length' => 200,
            'required' => true
        ],

        'description' => [
            'label' => 'Описания',
            'type' => 'TEXT'
        ],

        'image' => [
            'type' => 'VARCHAR',
            'length' => 50
        ],

        'qty' => [
            'label' => 'Количество',
            'type' => 'INT',
            'required' => true
        ],

        'price' => [
            'label' => 'Цена (грн)',
            'type' => 'INT',
            'required' => true
        ],

        'category' => [
            'label' => 'Категория',
            'type' => 'FK',
            'class' => 'models\Category',
            'required' => true
        ]
    ];

    public function tableName()
    {
        return 'products';
    }
}