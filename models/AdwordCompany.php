<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 14.10.15
 * Time: 13:40
 */

namespace models;


use app\sitebuilder\Model;

class AdwordCompany extends Model
{
    public $id;
    public $adword_id;
    public $company_id;
    public $access;


    public $fields = [
        'id' => [],
        'adword_id' => [],
        'company_id' => [],
        'access' => []
    ];

    public function tableName()
    {
        return 'adword_company';
    }
}