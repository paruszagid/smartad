<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 12.09.15
 * Time: 22:31
 */

namespace models;


use app\sitebuilder\Model;

class PostType extends Model
{
    public $fields = [
      'id' => [],
      'name' => [],
    ];

    public function tableName()
    {
        return 'post_types';
    }

    function __toString() {
        return $this->name;
    }
}