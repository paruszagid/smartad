<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 09.10.15
 * Time: 22:03
 */

namespace models;


use app\sitebuilder\Model;

class MediaPlan extends Model
{
    public $date_begin;
    public $date_end;
    public $adword_id;

    public $fields = [
        'id' => [],
        'date_begin' => [],
        'date_end' => [],
        'adword_id' => [],
    ];
    public function tableName()
    {
        return 'media_plan';
    }
}