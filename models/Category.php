<?
namespace models;

use app\sitebuilder\Model;

class Category extends Model {
    public $fields = [
        'id' => [
            'type' => 'INT'
        ],

        'title' => [
            'type' => 'VARCHAR',
            'length' => 150,
            'label' => 'Категория',
            'required' => true
        ],

        'description' => [
            'type' => 'TEXT',
            'label' => 'Описания'
        ]
    ];

    function __toString() {
        return $this->title;
    }

    public function tableName()
    {
        return 'categories';
    }
}