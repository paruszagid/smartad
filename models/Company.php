<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 22:21
 */

namespace models;


use app\sitebuilder\Model;

class Company extends Model
{

    public $fields = [
        'id' => [
            'type' => 'INT'
        ],

        'name' => [
            'type' => 'VARCHAR',
            'length' => 150,
            'label' => 'Название компании',
            'required' => true
        ],

        'image' => [
            'type' => 'VARCHAR',
            'length' => 50,
            'label' => 'Логотип',
            'required' => false
        ],

        'wifi' => [
            'type' => 'VARCHAR',
            'length' => 20,
            'label' => 'Wi-Fi',
            'required' => false
        ],

        'alias' => [
            'type' => 'VARCHAR',
            'length' => 150,
            'label' => 'Алиас',
            'required' => true
        ],

        'site' => [
            'type' => 'VARCHAR',
            'length' => 150,
            'label' => 'Сайт',
            'required' => false
        ],

        'date_begin' => [
            'type' => 'DATE',
            'label' => 'Дата начала контракта',
            'required' => true
        ],

        'date_end' => [
            'type' => 'DATE',
            'label' => 'Дата окончания контракта',
            'required' => true
        ],

        'fb' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'FaceBook',
            'required' => false
        ],

        'vk' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Vkontakte',
            'required' => false
        ],

        'shop' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'label' => 'Рекомендуем',
            'required' => false
        ],

        'telephone' => [
            'type' => 'VARCHAR',
            'length' => 100,
            'label' => 'Номер телефона',
            'required' => false
        ],

        'email' => [
            'type' => 'VARCHAR',
            'length' => 150,
            'label' => 'Email',
            'required' => false
        ],

    ];

    public function tableName()
    {
        return 'companies';
    }
}