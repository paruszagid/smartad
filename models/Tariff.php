<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 11.09.15
 * Time: 23:11
 */

namespace models;


use app\sitebuilder\Model;

class Tariff extends Model
{
    public $id;
    public $name;

    public $fields = [
        'id' => [],
        'name' => [],
        'per_month' => [],
        'posts_per_day' => [],
    ];

    public function tableName()
    {
        return 'tariffs';
    }

    function __toString() {
        return $this->name;
    }
}