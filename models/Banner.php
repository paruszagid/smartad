<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 07.10.15
 * Time: 21:16
 */

namespace models;


use app\sitebuilder\Model;

class Banner extends Model{
    public $id;
    public $url;
    public $image;
    public $company_id;

    public $fields = [
        'id' => [
            'type' => 'int'
        ],

        'url' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'required' => true
        ],

        'image' => [
            'type' => 'VARCHAR',
            'length' => 250,
            'required' => true
        ],

        'company_id' => [
            'type' => 'int'
        ],
    ];

    public function tableName()
    {
        return 'banners';
    }
}