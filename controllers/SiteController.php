<?php
/**
 * Created by PhpStorm.
 * User: serhiy
 * Date: 06.10.15
 * Time: 11:13
 */

namespace controllers;


use app\sbuser\User;
use app\sitebuilder\Database;
use app\sitebuilder\LayoutRender;
use app\sitebuilder\Render;
use app\sitebuilder\SiteBuilder;
use models\Banner;
use models\Client;
use models\Company;
use models\Post;
use models\Tariff;

class SiteController
{
    /**
     * Показує головну сторінку сайту
     * @return LayoutRender
     */
    function main_pageAction()
    {
        $time = time();

        $query = "SELECT DISTINCT a.* FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id
                WHERE m.date_begin <= $time and m.date_end >= $time
                ORDER BY RAND() LIMIT 4";

        $adwords = SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT);

        return new LayoutRender('landing/home', [
            'adwords' => $adwords
        ], 'newlanding');
    }

    function get_all_adwordsAction() {
        $time = time();
        $query = "SELECT DISTINCT a.* FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id
                WHERE m.date_begin <= $time and m.date_end >= $time
                ORDER BY RAND()";
        $adwords = SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT);

        return new Render('smartad/adwords', compact('adwords'));
    }

    function viewAction($alias)
    {
        $company = Company::where(['alias' => $alias])[0];
        $post = Post::where(['company_id' => $company->id])[0];
        $banners = Banner::where(['company_id' => $company->id]);

        if ($company)
            return new Render('smartad/start_page', [
                'company' => $company,
                'post' => $post,
                'banners' => $banners
            ]);
    }

    function companyAction($alias)
    {
        if (!isset($_COOKIE['not_first_time'])) {
            setcookie('not_first_time', true, time() + (30 * 60), '/');
            return self::viewAction($alias);
        } else {
            return self::adwordAction($alias);
        }
    }

    function adwordAction($alias)
    {
        $time = time();
        $company = Company::where(['alias' => $alias],['fields' => ['id', 'site']])[0];
        $post = Post::where(['company_id' => $company->id, ['fields' => 'link']])[0];

        $adword_company = SiteBuilder::$app->db->getResultQuery(
            "SELECT c.id, ac.access, ac.adword_id FROM companies c JOIN adword_company ac ON c.id = ac.company_id",
            Database::$RESULT_OBJECT
        );

        if (count($adword_company) == 0) {
            $query = "SELECT DISTINCT a.* FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id
                WHERE m.date_begin <= $time and m.date_end >= $time
                ORDER BY RAND() LIMIT 4";

            $adwords = SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT);
        } else {
            $query = "SELECT DISTINCT a.* FROM media_plan as m
                JOIN adwords as a on a.id = m.adword_id
                WHERE m.date_begin <= $time and m.date_end >= $time
                ORDER BY RAND()";

            $adwords = SiteBuilder::$app->db->getResultQuery($query, Database::$RESULT_OBJECT);

            foreach ($adwords as $adword) {
                foreach ($adword_company as $ac) {
                    if ($ac->adword_id == $adword->id) {
                        if ($ac->access == '0' and $ac->id == $company->id) {
                            unset($adwords[array_search($adword, $adwords)]);
                            break;
                        } elseif ($ac->access == '1') {
                            if ($ac->id !== $company->id) {
                                unset($adwords[array_search($adword, $adwords)]);
                                break;
                            }
                        }
                    }
                }
            }

            array_slice($adwords, 0, 4);
        }

        return new Render('smartad/adword', [
            'adwords' => $adwords,
            'company' => $company,
            'post' => $post
        ]);
    }
}