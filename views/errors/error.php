<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>

    <title><?= $exception->getMessage();?></title>
</head>

<body>
<div class="header">
    <h1><?= $exception->getMessage();?></h1>
</div>

<div class="call-stack">

    <ul style="list-style: none;">
        <?= $handler->renderCallStackItem($exception->getFile(), $exception->getLine(), null, null, [], 1) ?>
        <?php for ($i = 0, $trace = $exception->getTrace(), $length = count($trace); $i < $length; ++$i): ?>
            <?= $handler->renderCallStackItem(@$trace[$i]['file'] ?: null, @$trace[$i]['line'] ?: null,
                @$trace[$i]['class'] ?: null, @$trace[$i]['function'] ?: null, $trace[$i]['args'], $i + 2) ?>
        <?php endfor; ?>
    </ul>
</div>

<script type="text/javascript">SyntaxHighlighter.all();</script>

</body>

</html>

