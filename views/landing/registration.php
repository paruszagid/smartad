<?$this->title = "Регистрация"?>
<form action="/registration" id="registerForm" method="post" onsubmit="return valid()">
    <div class="main">
        <div class="container">
            <br>
            <div id="alerts" >
            </div>
            <div class="register">
                <div class="register-top-grid">
                    <h3>Регистрация. Тарифный план "<?=$tariff->name?>"</h3>
                    <input type = "hidden" name = 'tariff' value = "<?=$tariff->id?>"/>
                    <div class="wow fadeInLeft" data-wow-delay="0.4s">
                        <span>Имя<label>*</label></span>
                        <input type="text" name="first_name" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Фамилия<label>*</label></span>
                        <input type="text" name="sur_name" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Отчество<label>*</label></span>
                        <input type="text" name="last_name" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Email<label>*</label></span>
                        <input type="email" name="email" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Номер телефона<label>*</label></span>
                        <input type="tel" name="telephone" required>
                    </div>

                    <div class="wow fadeInLeft" data-wow-delay="0.4s">
                        <span>Логин<label>*</label></span>
                        <input type="text" id = "login" name="login" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Пароль<label>*</label></span>
                        <input type="text" name="password" required>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>

                <div class="register-top-grid">
                    <h3>Информация о компании</h3>

                    <div class="wow fadeInLeft" data-wow-delay="0.4s">
                        <span>Название компании<label>*</label></span>
                        <input type="text" id="title" name="name" required>
                    </div>

                    <div class="wow fadeInRight" data-wow-delay="0.4s">
                        <span>Алиас<label>*</label></span>
                        <input type="text" id="alias" name="alias" required>
                    </div>


                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="register-but">
                    <input type="submit" value="Регистрация">

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function translit(){
        // Символ, на который будут заменяться все спецсимволы
        var space = '-';
        // Берем значение из нужного поля и переводим в нижний регистр
        var text = $('#title').val().toLowerCase();
        //var text = document.getElementById('name').value.toLowerCase();
        // Массив для транслитерации
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': space, 'ы': 'y',
            'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',

            ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space, '#': space, '$': space,
            '%': space, '^': space, '&': space, '*': space, '(': space, ')': space, '-': space, '\=': space,
            '+': space, '[': space, ']': space, '\\': space, '|': space, '/': space, '.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space, '?': space, '<': space,
            '>': space, '№': space, '«'	: space, '»': space
        }

        var result = '';

        var curent_sim = '';

        for(i=0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if(transl[text[i]] != undefined) {
                if(curent_sim != transl[text[i]] || curent_sim != space){
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }

        result = TrimStr(result);

        // Выводим результат
        $('#alias').val(result);
        //document.getElementById('alias').value = result;

    }
    function TrimStr(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }

    // Выполняем транслитерацию при вводе текста в поле
    $(function(){
        $('#title').keyup(function(){
            translit();
            return false;
        });
    });

    function valid() {
        $.post('/validation', $('#registerForm').serialize(), function(data) {
            if (data.length == 0) {
                $.post(location.href, $('#registerForm').serialize(), function(data) {
                    $('.container')
                        .html(data);
                });
            } else {
                $('#alerts').html("");

                data.forEach(function(item) {
                    $('#alerts').append(
                        $('<div>')
                            .addClass('alert')
                            .addClass('alert-danger')
                            .html(item)
                    )
                });
            }
        }, 'json');
        return false;
    }
</script>
