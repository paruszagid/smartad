<? $this->title = \app\sitebuilder\SiteBuilder::$app->siteName; ?>
<script src="/assets/libs/readmore.js"></script>
<script src="/assets/libs/masonry.pkgd.min.js"></script>
<style>
    .adword {
        background: #fff;
        -webkit-box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);
        border-radius: 9px;
    }

    .card .card-content {
        padding: 5px;
        border-radius: 0 0 2px 2px;
    }

    .card .card-title {
        font-size: 20px;
        font-weight: normal;
    }

    .number {
        position: relative;
        top: -45px;
        left: 40%;
        font-size: 20px;
    }

    .card-panel {
        margin-top: 30px;
    }
</style>

<div class="center-align" style="position: absolute; top: 20px; width: 100%;">
    <img src="/assets/img/icon.ico" width="100">
    <h4 class="white-text">Клиенты саме расскажут о Вас !!!</h4>
</div>

<div class="parallax-container" style="height: 300px;">
    <div class="parallax">
        <img src="/assets/landing/images/banner.jpg">
    </div>
</div>

<div class="container">
    <h4 align="center">Рекламма, которая сейчас показывается</h4>

    <div class="row" id="adwords">
        <? foreach ($adwords as $adword): ?>
            <div class="col m6 s6 l3">
                <div class="card adword">
                    <? if ($adword->video): ?>
                        <div class="video-container">
                            <?= $adword->video ?>
                        </div>
                    <? elseif ($adword->image): ?>
                        <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                        <div class="card-image">
                            <img src="/content/adwords/images/<?= $adword->image ?>">
                        </div>
                        <? if ($adword->link): ?></a><? endif ?>
                    <? endif ?>
                    <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                        <h3 class="card-title black-text center"><?= $adword->title ?></h3>
                        <? if ($adword->link): ?></a><? endif ?>


                    <div class="card-content">
                        <p class="black-text"><?= $adword->text ?></p>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>

    <div class="clearfix"></div>
    <div class="center-align">
        <button class="btn blue" id="loadMore"><i class="mdi-navigation-expand-more"></i> Загрузить еще</button>
    </div>
    <div class="clearfix"></div>
</div>

<div class="parallax-container" style="height: 100px;">
    <div class="parallax">
        <img src="/assets/landing/images/banner.jpg">
    </div>
</div>

<div class="container">
    <h4 align="center">Как это работает</h4>

    <div class="row">
        <div class="col s12 m12 l4">
            <div class="card-panel blue-grey white-text">
                <button class="btn-floating btn-large center-aling number grey">1</button>
                <div class="card-content center-align">
                    Гость подключается к Wi-Fi сети и попадает на стартовую страницу системы SmartAd.
                </div>
            </div>
        </div>
        <div class="col s12 m12 l4">
            <div class="card-panel blue-grey white-text">
                <button class="btn-floating btn-large center-aling number grey">2</button>
                <div class="card-content center-align">
                    На стартовой странице расположенна информация о заведении (название, телефон, адрес, ссылка на
                    web-сайт),
                    рекламный баннер заведения, социальные кнопки, кнопка реклама и кнопка рекомендуем.
                </div>
            </div>
        </div>
        <div class="col s12 m12 l4">
            <div class="card-panel blue-grey white-text">
                <button class="btn-floating btn-large center-aling number grey">3</button>
                <div class="card-content center-align">
                    Для доступа в интернет пользователь должен нажать одну из кнопок : реклама, одна из соц.сетей,
                    кнопка
                    рекомендации
                </div>
            </div>
        </div>
    </div>
</div>


<div class="parallax-container" style="height: 100px;">
    <div class="parallax">
        <img src="/assets/landing/images/banner.jpg">
    </div>
</div>

<div class="container">
    <h4 align="center">Тарифные планы</h4>

    <div class="row" id="tarrifs">
        <div class="col m12 s12 l6">
            <div class="card-panel grey">
                <div class="card-content white center-align" style="padding: 10px;">
                    <b>Выгодный старт - $10/месяц</b>
                </div>

                <div class="card-content">
                    <ul class="collection">
                        <li class="collection-item">
                            Реклама на стартовой странице (название и координаты заведения) в виде размещения
                            рекламного банера
                        </li>
                        <li class="collection-item">
                            Связь соц сетями ВК и Facebook для ре-постов (один вид рекламного поста в месяц)
                        </li>
                        <li class="collection-item">
                            Регистрация страницы в ВК и Facebook (если нет) и наполнение ее информацией
                        </li>
                        <li class="collection-item">
                            Переход после ре-поста на сайт клиента или заданную страницу
                        </li>
                        <li class="collection-item">
                            Размещение до 2-х рекламных банеров, видео или фотографии в разделе реклама
                        </li>
                        <li class="collection-item">
                            Настройка оборудования клиента (оборудование дается бесплатно во временное пользование)
                        </li>
                        <li class="collection-item">
                            Техническая поддержка, консультации по размещению, гарантия работоспособности
                        </li>
                        <li class="collection-item">
                            Разработка рекламных постов и банеров оплачивается отдельно
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col m12 s12 l6">
            <div class="card-panel blue">
                <div class="card-content white center-align" style="padding: 10px;">
                    <b>Выгодный - $20/месяц</b>
                </div>

                <div class="card-content">
                    <ul class="collection">
                        <li class="collection-item">
                            Реклама на стартовой странице (название и координаты заведения) в виде размещения
                            рекламного банера с возможностью перехода при нажатие на нем
                        </li>
                        <li class="collection-item">
                            Связь соц сетями ВК и Facebook для ре-постов (до четырех видов рекламных постов в месяц)
                        </li>
                        <li class="collection-item">
                            Регистрация страницы в ВК и Facebook (если нет) и наполнение ее информацией
                        </li>
                        <li class="collection-item">
                            Переход после ре-поста на сайт клиента или заданную страницу
                        </li>
                        <li class="collection-item">
                            Размещение до 4-х рекламных банеров, видео или фотографии в разделе реклама
                        </li>
                        <li class="collection-item">
                            Настройка оборудования клиента (оборудование дается бесплатно во временное пользование)
                        </li>
                        <li class="collection-item">
                            Техническая поддержка, консультации по размещению, гарантия работоспособности
                        </li>
                        <li class="collection-item">
                            Разработка рекламных постов и банеров 4 шт в месяц, все что выше оплачивается отдельно
                        </li>

                        <li class="collection-item">
                            <b>Cкидка на размещения рекламы заведения на чужих площадках - 5%</b>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col m12 s12 l6">
            <div class="card-panel green">
                <div class="card-content white center-align" style="padding: 10px;">
                    <b>Супер выгодный - $30/месяц</b>
                </div>

                <div class="card-content">
                    <ul class="collection">
                        <li class="collection-item">
                            Реклама на стартовой странице (название и координаты заведения) в виде размещения
                            рекламного банера с возможностью перехода при нажатие на нем
                        </li>
                        <li class="collection-item">
                            Связь соц сетями ВК и Facebook для ре-постов (до четырех видов рекламных постов в месяц)
                        </li>
                        <li class="collection-item">
                            Регистрация страницы в ВК и Facebook (если нет) и наполнение ее информацией
                        </li>
                        <li class="collection-item">
                            Переход после ре-поста на сайт клиента или заданную страницу
                        </li>
                        <li class="collection-item">
                            Размещение до 8-х рекламных банеров, видео или фотографии в разделе реклама
                        </li>
                        <li class="collection-item">
                            Настройка оборудования клиента (оборудование дается бесплатно во временное пользование)
                        </li>
                        <li class="collection-item">
                            Техническая поддержка, консультации по размещению, гарантия работоспособности
                        </li>

                        <li class="collection-item">
                            <b>Дополнительные работы с соц сетями:</b>
                        </li>

                        <li class="collection-item">
                            - Создание мероприятий, акций и рассылка контактам
                        </li>

                        <li class="collection-item">
                            - Анти троллинг (сбор и отслеживание комментариев о Вас и удаления не нужных
                        </li>

                        <li class="collection-item">
                            - Приглашение пользователей ( «друзей» )
                        </li>

                        <li class="collection-item">
                            - Обновление фото и видео( предоставленного вами)
                        </li>

                        <li class="collection-item">
                            - Рассылка новостей компании
                        </li>

                        <li class="collection-item">
                            - Разработка постов для последующих ре постов
                        </li>
                        <li class="collection-item">
                            <b>Cкидка на размещения рекламы заведения на чужих площадках - 10%</b>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col m12 s12 l6">
            <div class="card-panel red">
                <div class="card-content white center-align" style="padding: 10px;">
                    <b>Максимально выгодный - $40/месяц</b>
                </div>

                <div class="card-content">
                    <ul class="collection">
                        <li class="collection-item">
                            Реклама на стартовой странице (название и координаты заведения) в виде размещения
                            рекламного банера с возможностью перехода при нажатие на нем
                        </li>
                        <li class="collection-item">
                            Связь соц сетями ВК и Facebook для ре-постов (до четырех видов рекламных постов в месяц)
                        </li>
                        <li class="collection-item">
                            Регистрация страницы в ВК и Facebook (если нет) и наполнение ее информацией
                        </li>
                        <li class="collection-item">
                            Переход после ре-поста на сайт клиента или заданную страницу
                        </li>
                        <li class="collection-item">
                            Размещение до 10-х рекламных банеров, видео или фотографии в разделе реклама
                        </li>
                        <li class="collection-item">
                            Настройка оборудования клиента (оборудование дается бесплатно во временное пользование)
                        </li>
                        <li class="collection-item">
                            Техническая поддержка, консультации по размещению, гарантия работоспособности
                        </li>

                        <li class="collection-item">
                            <b>Дополнительные работы с соц сетями:</b>
                        </li>

                        <li class="collection-item">
                            - Создание мероприятий, акций и рассылка контактам
                        </li>

                        <li class="collection-item">
                            - Анти троллинг (сбор и отслеживание комментариев о Вас и удаления не нужных
                        </li>

                        <li class="collection-item">
                            - Приглашение пользователей ( «друзей» )
                        </li>

                        <li class="collection-item">
                            - Обновление фото и видео( предоставленного вами)
                        </li>

                        <li class="collection-item">
                            - Рассылка новостей компании
                        </li>

                        <li class="collection-item">
                            - Разработка постов для последующих ре постов
                        </li>
                        <li class="collection-item">
                            <b>Предоставление статистики по клиентам</b>
                        </li>
                        <li class="collection-item">
                            <b>Размещение своей инф. или рекламы в разделе “рекомендации”</b>
                        </li>
                        <li class="collection-item">
                            <b>Cкидка на размещения рекламы заведения на чужих площадках - 15%</b>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="parallax-container" style="height: 100px;">
    <div class="parallax">
        <img src="/assets/landing/images/banner.jpg">
    </div>
</div>

<div class="container">
    <h4 align="center">Сравнение тарифов</h4>

    <table class="table centered striped table-responsive">
        <thead>
        <tr>
            <td>№</td>
            <td>Вид услуг</td>
            <td>Выгодный старт</td>
            <td>Выгодный</td>
            <td>Супер Выгодный</td>
            <td>Максимально Выгодный</td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>1</td>
            <td>Рекламный  банер с возможностью перехода при нажатие на нем</td>
            <td>-</td>
            <td>+</td>
            <td>+</td>
            <td>+</td>
        </tr>

        <tr>
            <td>2</td>
            <td>Связь c соц. сетями ВК и Facebook для ре-постов</td>
            <td>1</td>
            <td>4</td>
            <td>8</td>
            <td>10</td>
        </tr>

        <tr>
            <td>3</td>
            <td>Размещение  рекламных  банеров, видео  или фотографии в разделе реклама</td>
            <td>2</td>
            <td>4</td>
            <td>8</td>
            <td>10</td>
        </tr>

        <tr>
            <td>4</td>
            <td>Дополнительные работы с соц сетями</td>
            <td>-</td>
            <td>-</td>
            <td>+</td>
            <td>+</td>
        </tr>

        <tr>
            <td>5</td>
            <td>Размещение своей инф. или рекламы в разделе “рекомендации”</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>+</td>
        </tr>

        <tr>
            <td>6</td>
            <td>Предоставление статистики по клиентам</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>+</td>
        </tr>

        <tr>
            <td><b>7</b></td>
            <td><b>Скидка на размещения рекламы  заведения  на чужих  площадках</b></td>
            <td><b>0</b></td>
            <td><b>5%</b></td>
            <td><b>10%</b></td>
            <td><b>15%</b></td>
        </tr>

        <tr>
            <td><b>8</b></td>
            <td><b>Цена</b></td>
            <td><b>10$ (предоплата 3 месяца)</b></td>
            <td><b>20$ (предоплата 2 месяца)</b></td>
            <td><b>30$</b></td>
            <td><b>40$</b></td>
        </tr>
        </tbody>
    </table>
</div>

<script>
    $(function () {

        function less_text() {
            $('.card-content p').readmore({
                speed: 100,
                moreLink: '<a href="#"><i class="mdi-navigation-expand-more"></i> Развернуть</a>',
                lessLink: '<a href="#"><i class="mdi-navigation-expand-less"></i> Свернуть</a>',
                collapsedHeight: 50,
                afterToggle: meansory_adwords
            });
        }

        function meansory_adwords() {
            $('#adwords').masonry({
                // options
                itemSelector: '.col'
            });
        }

        less_text();
        meansory_adwords();

        $('#loadMore').click(function () {
            $('#adwords').fadeOut();

            $.get('/get_all_adwords', {}, function (d) {
                $('#adwords').html(d);
                $('#adwords').fadeIn();
                $('#loadMore').hide();

                less_text();
                meansory_adwords();
            });
        });

        $('.parallax').parallax();

        $('#tarrifs').masonry({
            // options
            itemSelector: '.col'
        });
    });
</script>