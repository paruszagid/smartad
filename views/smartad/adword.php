<!DOCTYPE html>
<html>
<head>
    <title>SmartAd - рекламма</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
    <script src="/assets/jquery/jquery.min.js"></script>
    <script src="/assets/materialize/js/materialize.js"></script>
    <script src="/assets/libs/readmore.js"></script>
    <script src="/assets/libs/masonry.pkgd.min.js"></script>
    <style>

        audio {
            width: 100%;
            padding: 0;
            border: none;
            border-radius: 0px;
        }

        .card {
            background: #fff;
            -webkit-box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);
            box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);
            border-radius: 9px;
        }

        .card .card-content {
            padding: 5px;
            border-radius: 0 0 2px 2px;
        }

        .card .card-title {
            font-size: 20px;
            font-weight: normal;
        }

        .fixed-action-btn ul {
            left: 45px;
            right: 0;
            text-align: center;
            position: absolute;
            bottom: 20px;
            margin: 0;
        }
    </style>
</head>
<body class="grey lighten-4">
<nav class="navbar-fixed blue">
    <a class="brand-logo center">SmartAd - рекламма</a>
</nav>

<div class="container">
    <div class="content row center-align">
        <h4 style=" font-size: 20px;">Рекламму можно будет пропустить через <span id="second">15</span></h4>

        <button id="skip" class="btn red hide">Пропустить рекламму <i class="mdi-image-navigate-next"></i></button>
    </div>

    <div class="content row" id="adwords">
        <? foreach ($adwords as $adword): ?>
            <div class="col m6 s6 l3">
                <div class="card">
                    <? if ($adword->video): ?>
                        <div class="video-container">
                            <?= $adword->video ?>
                        </div>
                    <? elseif ($adword->image): ?>
                        <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                        <div class="card-image">
                            <img src="/content/adwords/images/<?= $adword->image ?>">
                        </div>
                        <? if ($adword->link): ?></a><? endif ?>
                    <? endif ?>
                    <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                        <h3 class="card-title black-text center"><?= $adword->title ?></h3>
                        <? if ($adword->link): ?></a><? endif ?>


                    <div class="card-content">
                        <p class="black-text"><?= $adword->text ?></p>
                    </div>

                    <div class="card-action right-align">
                        <? if ($adword->audio): ?>
                            <audio id="audio" src="/content/adwords/audios/<?= $adword->audio ?>"
                                   preload="none"></audio>
                            <button id="controll" class="btn red"><i class="mdi-av-play-circle-outline"></i></button>
                        <? endif ?>

                        <!--<div class="fixed-action-btn right">
                                <a class="btn blue white-text">
                                    <i class="mdi-social-share"></i>
                                </a>

                            </div>-->
                        <? if ($adword->link): ?>
                            <div class="fixed-action-btn">
                                <a class="btn blue white-text">
                                    <i class="mdi-social-share"></i>
                                </a>
                                <ul>
                                    <ul>
                                        <li>
                                            <a data-link="<?= $adword->link ?>"
                                               data-sendto="http://vk.com/share.php?url="
                                               class="share btn-large btn-floating blue lighten-2 white-text">
                                                <i class="fa fa-vk"></i>
                                            </a>
                                        </li>
                                        <li >
                                            <a data-link="<?= $adword->link ?>"
                                               data-sendto="http://www.facebook.com/sharer.php?u="
                                               class="share btn-large btn-floating blue white-text">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a data-link="<?= $adword->link ?>"
                                               data-sendto="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl="
                                               class="share btn-large btn-floating orange white-text">
                                                <i class="fa fa-odnoklassniki"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </ul>
                            </div>
                        <? endif ?>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>
</div>

<footer class="page-footer blue">
    <div class="footer-copyright">
        <div class="row">
            <div class="col l4 center-align">
                <i class="fa fa-phone"></i> +38 (050) 434 0 333, +38 (067) 372 2 767
            </div>

            <div class="col l4 center-align">
                <a href="/"><img src="/assets/img/icon.ico" width="50"></a>
            </div>

            <div class="col l4 center-align">
                <i class="fa fa-envelope"></i> <a href="mailto:info@sisoftware.biz" class="white-text">info@sisoftware.biz</a>
            </div>
        </div>
    </div>
</footer>

<script>
    $(function () {
        var played = false;
        $('#controll').on('click', function () {
            var audio = document.getElementById('audio')
            if (!played) {
                audio.play();
                played = true;
            } else {
                audio.pause();
                played = false;
            }

            $(this).children().first().toggleClass('mdi-av-play-circle-outline')
            $(this).children().first().toggleClass('mdi-av-pause-circle-outline')
            $(this).toggleClass("blue");
            $(this).toggleClass("red");

        });

        $('.card-content p').readmore({
            speed: 100,
            moreLink: '<a href="#"><i class="mdi-navigation-expand-more"></i> Развернуть</a>',
            lessLink: '<a href="#"><i class="mdi-navigation-expand-less"></i> Свернуть</a>',
            collapsedHeight: 30,
            afterToggle: function () {
                $('#adwords').masonry({
                    // options
                    itemSelector: '.col'
                });
            }
        });

        $('#adwords').masonry({
            // options
            itemSelector: '.col'
        });

        $('.share').on('click', function () {
            var w = window.open($(this).data('sendto') + encodeURIComponent($(this).data('link')), '', 'toolbar=0,status=0,width=626,height=436');
        });

        var seconds = 15;

        setInterval(function() {
            seconds--;
            if (seconds < 0)
                return;

            if (seconds == 0) {
                $('#second').parent().toggleClass('hide');
                $('#skip').toggleClass('hide');

                $('#skip').on('click', function() {
                    location.href = '<?=($post->link? $post->link : $company->site)?>';
                });
            } else {
                $('#second').html(seconds);
            }
        }, 1000);
    });
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68697685-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>