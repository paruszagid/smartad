<? foreach ($adwords as $adword): ?>
    <div class="col m6 s6 l3">
        <div class="card">
            <? if ($adword->video): ?>
                <div class="video-container">
                    <?= $adword->video ?>
                </div>
            <? elseif ($adword->image): ?>
                <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                <div class="card-image">
                    <img src="/content/adwords/images/<?= $adword->image ?>">
                </div>
                <? if ($adword->link): ?></a><? endif ?>
            <? endif ?>
            <? if ($adword->link): ?><a href="<?= $adword->link ?>" target="_blank"><? endif ?>
                <h3 class="card-title black-text center"><?= $adword->title ?></h3>
                <? if ($adword->link): ?></a><? endif ?>


            <div class="card-content">
                <p class="black-text"><?= $adword->text ?></p>
            </div>
        </div>
    </div>
<? endforeach ?>