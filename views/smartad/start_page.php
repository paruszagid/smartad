<!DOCTYPE html>
<html>
<head>
    <title><?= $post->company_id->name ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/reslider.css">
    <script src="/assets/jquery/jquery.min.js"></script>
    <script src="/assets/materialize/js/materialize.js"></script>
    <script src="/assets/libs/reslider.js"></script>
    <script src="https://connect.facebook.net/en_US/all.js"></script>
    <style>
        .content {
            min-height: calc(100vh - 154px);
            padding-top: 10px;
        }
    </style>
</head>
<body>
<nav class="navbar-fixed blue z-depth-2" style="height: 85px;">
    <a <?if ($company->site):?>href="<?=$company->site?>" <?endif?> class="brand-logo center thin"><img style="height: 50px;" class="responsive-img"
                                                                                                        src="/content/companies/<?= $company->image ?>"></a>
    <a class="brand-logo center thin" style="top: 35px;">
        <small><?= $post->company_id->name ?></small>
    </a>
</nav>

<div class=" content">
    <div class="center">
        <h4 class="">Для доступа в интернет нужно выбрать соц. сеть или просмотреть рекламу</h4>

        <a href="/adword" class="btn waves-effect  btn-large orange tooltipped"
           data-tooltip="Посмотреть рекламу"><i class="fa fa-commenting-o"></i> Смотреть рекламму</a>

        <? if ($post->company_id->shop): ?>
            <a href="/recommendations" class="btn waves-effect  btn-large grey tooltipped"
               data-tooltip="Рекомендуем"><i class="fa fa-angellist"></i> Рекоммендации</a>
        <? endif ?>

        <br>
        <br>
        <? if ($post->vk): ?>
            <button class="btn-floating btn-large waves-effect blue tooltipped social" data-social="vk"
                    data-tooltip="Сделать репост в VK"><i
                    class="fa fa-vk"></i></button>
        <? endif ?>

        <? if ($post->fb): ?>
            <button class="btn-floating btn-large waves-effect green tooltipped social" data-social="fb"
                    data-tooltip="Сделать репост в Facebook"><i
                    class="fa fa-facebook"></i></button>
        <? endif ?>

        <? if ($post->ok): ?>
            <button class="btn-floating btn-large waves-effect orange tooltipped social" data-social="ok"
                    data-tooltip="Сделать репост в Odnoklassniki"><i
                    class="fa fa-odnoklassniki"></i></button>
        <? endif ?>

        <? if ($post->tw): ?>
            <button class="btn-floating btn-large waves-effect grey tooltipped social" data-social="tw"
                    data-tooltip="Сделать репост в Twitter"><i
                    class="fa fa-twitter"></i></button>
        <? endif ?>
    </div>
    <br>

    <div class="row">


        <div class="col m6 offset-m3">
            <ul class="rslides">
                <? foreach ($banners as $banner): ?>
                    <li>
                        <? if ($banner->url): ?>
                            <a href="<?= $banner->url ?>"><img src="/content/banners/<?= $banner->image ?>"></a>
                        <? else: ?>
                            <img src="/content/banners/<?= $banner->image ?>">
                        <? endif ?>
                    </li>
                <? endforeach ?>
            </ul>
        </div>
    </div>

    <div class="row">
        <?if($company->email):?>
            <div class="center">
                <span><i class="mdi-communication-email"></i> <a href="mailto:<?=$company->email?>"><?=$company->email?></a> </span>
                <span><i class="mdi-communication-phone"></i><?=$company->telephone?></span>
            </div>
        <?endif?>
    </div>
</div>

<footer class="page-footer blue">
    <div class="footer-copyright">
        <div class="row">
            <div class="col l4 center-align">
                <i class="fa fa-phone"></i> +38 (050) 434 0 333, +38 (067) 372 2 767
            </div>

            <div class="col l4 center-align">
                <a href="/"><img src="/assets/img/icon.ico" width="50"></a>
            </div>

            <div class="col l4 center-align">
                <i class="fa fa-envelope"></i> <a href="mailto:info@sisoftware.biz" class="white-text">info@sisoftware.biz</a>
            </div>
        </div>
    </div>
</footer>

<script>
    $(function () {
        $('.tooltipped').tooltip({delay: 50, position: 'top'});
        $('.slider').slider();
        $('.indicators').hide();

        var ok_link = '<?= $post->ok?>';
        var tw_link = '<?= $post->tw?>';
        var vk_link = '<?= $post->vk?>';
        var fb_link = '<?= $post->fb?>';

        FB.init({
            appId: "721223471286937", status: true, cookie: true
        });


        $('.social').on('click', function () {
            switch ($(this).data('social')) {
                case 'ok':
                    location.href = ok_link;
                    break;

                case 'tw':
                    location.href = tw_link;
                    break;

                case 'vk':
                    location.href = vk_link;
                    break;

                case 'fb': {
                    var obj = {
                        method: 'feed',
                        redirect_uri: 'http://restoran-elita.lviv.ua/',
                        link:'http://restoran-elita.lviv.ua/',
                        picture: 'http://smartad.sisoftware.biz/content/cp/posts/d87005b7b76207da6bef0b17a61f44a3jpeg',
                        name: '',
                        caption: 'Вы попали в настоящий украинский народный ресторан, в котором вам предложат только лучшие национальные блюда',
                        description: ''
                    };

                    function callback(response) {
                        if (response && !response.error) {
                            Materialize.toast("Доступ в интернет открыт")
                        } else {
                            Materialize.toast("Доступ в интернет закрыт");
                        }
                    }

                    FB.getLoginStatus(function (response) {
                        if (response.status === 'connected') {

                            alert("Connecter");
                            FB.ui(obj, callback);

                        } else {
                            alert("Not connected")
                            location.href = fb_link;
                        }
                    });


                }
                    break;
            }
        });

        $(".rslides").responsiveSlides({
            auto: true,             // Boolean: Animate automatically, true or false
            speed: 500,            // Integer: Speed of the transition, in milliseconds
            timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
            pager: false,           // Boolean: Show pager, true or false
            nav: false,             // Boolean: Show navigation, true or false
            random: false,          // Boolean: Randomize the order of the slides, true or false
            pause: false,           // Boolean: Pause on hover, true or false
            pauseControls: true,    // Boolean: Pause when hovering controls, true or false
            prevText: "Previous",   // String: Text for the "previous" button
            nextText: "Next",       // String: Text for the "next" button
            maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
            navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
            manualControls: "",     // Selector: Declare custom pager navigation
            namespace: "rslides",   // String: Change the default namespace used
            before: function(){},   // Function: Before callback
            after: function(){}     // Function: After callback
        });
    });


</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68697685-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>